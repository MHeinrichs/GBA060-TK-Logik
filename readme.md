
# GBA060-TK-Logik

This is an alternative firmware for Georg Brauns 68060-turbocard for a 68030-CPU-Slot.

It implements bus sizing, 68030/68060 cycle conversion and a SD-RAM-Interface with movem-bursts.

Since the PCB is under Georg's copyright its quite useless, but i work with it on the existing cards.

Fell free to use it for your projects.

